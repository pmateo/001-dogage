//
//  ViewController.swift
//  EdadPerro
//
//  Created by formador on 21/11/16.
//  Copyright © 2016 formador. All rights reserved.
//

import UIKit

// 002 - Esconder teclado con return Key
// Creamos el Delegate
class ViewController: UIViewController, UITextFieldDelegate {
    
    // Outlets
    @IBOutlet weak var myTextField: UITextField!
    @IBOutlet weak var textoFinal: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // 002 - Asignamos el Delegate
        self.myTextField.delegate = self
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Acciones
    @IBAction func calcularEdad(_ sender: AnyObject) {
        let edad = 7 * Int(myTextField.text!)!
        textoFinal.text = "Tu perro tiene \(edad) años humanos"
    }
    
    // 001 - Esconder teclado
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
        //super.touchesBegan(touches, with: event)
    }
    
    // 002 - Esconder teclado con return key
    func textFieldShouldReturn(_ textField: UITextField) -> Bool // called when 'return' key pressed. return false to ignore.
    {
        textField.resignFirstResponder()
        calcularEdad(self)
        return true
    }
    
    
}
